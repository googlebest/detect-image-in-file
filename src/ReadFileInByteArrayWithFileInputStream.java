

import java.awt.image.BufferedImage;
import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;

import javax.imageio.ImageIO;

public class ReadFileInByteArrayWithFileInputStream {
	
	public static void main(String[] args) throws IOException {
		byte fileContent[] = null;
		byte[] pattern = hexStringToByteArray("FFD8FFE0");
		byte[] pattern2 = hexStringToByteArray("89504E47");
		for (int i = 100; i< 420; i++){
		File file = new File("D:/850-root/lv0"+i+".dat");
		FileInputStream fin = null;
		try {
			// create FileInputStream object
			fin = new FileInputStream(file);

			
			byte fileContent2[] = new byte[200];
			// Reads up to certain bytes of data from this input stream into an array of bytes.
			KMPMatch km = new KMPMatch();
			fin.read(fileContent2);

			int i2 = km.indexOf(fileContent2, pattern);
			if (i2 <= 0) 
				i2 = km.indexOf(fileContent2, pattern2);
			fileContent = new byte[(int) file.length()-i2+1];
			fin.close();
			fin = new FileInputStream(file);

			System.out.println("File content:1 " + i2);
			fin.skip(i2);
			fin.read(fileContent);
		//	fileContent = new byte[(int) file.length()];
			
			
			
			//BufferedImage image = ImageIO.read( new ByteArrayInputStream( fileContent ) );
			//ImageIO.write(image, "BMP", new File("filename.bmp"));
			//create string from byte array
		String s = new String(fileContent);
			System.out.println("File content: " + s);
			
			
			
		}
		catch (FileNotFoundException e) {
			System.out.println("File not found" + e);
		}
		catch (IOException ioe) {
			System.out.println("Exception while reading file " + ioe);
		}
		
		
		
		finally {
			// close the streams using close method
			try {
				if (fin != null) {
					fin.close();
				}
			}
			catch (IOException ioe) {
				System.out.println("Error while closing stream: " + ioe);
			}
		}
		
		FileOutputStream fos = new FileOutputStream("D:/850-root/outout/"+i+".jpg");
		fos.write(fileContent);
		fos.close();
		fin.close();
		}
	}
	
	public static byte[] hexStringToByteArray(String s) {
	    int len = s.length();
	    byte[] data = new byte[len / 2];
	    for (int i = 0; i < len; i += 2) {
	        data[i / 2] = (byte) ((Character.digit(s.charAt(i), 16) << 4)
	                             + Character.digit(s.charAt(i+1), 16));
	    }
	    return data;
	}
}